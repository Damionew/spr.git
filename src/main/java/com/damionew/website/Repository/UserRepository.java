package com.damionew.website.Repository;

import org.springframework.data.repository.CrudRepository;

import com.damionew.website.model.User;

public interface UserRepository extends CrudRepository<User, Long>{
    
}
