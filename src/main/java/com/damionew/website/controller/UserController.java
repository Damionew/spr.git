package com.damionew.website.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.damionew.website.Repository.UserRepository;
import com.damionew.website.model.User;

@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserRepository userRepository;
    @RequestMapping("/test")
    public String Test() {
	User user = new User();
	user.setId(1);
	user.setName("zhangsan");
	user.setEmail("zhangsan@zhangsan.com");
	userRepository.save(user);
	return "sleep";
    }
}
