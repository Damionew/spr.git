/**
 * @author yinyunqi
 * @datetime 2017��11��18��
 */
package com.damionew.website.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping
public class UrlController {
	@RequestMapping("/")
	@ResponseBody
	public String hello() {
		System.out.println("index");
		return "sleep";
	}
	@RequestMapping("/test")
	public String indexTest(Model model) {
		System.out.println("index");
		return "sleep";
	}
}
